#include <iostream>
#include <fstream>

/**
 * Entry point
 *
 * @param argc Arguments count
 * @param argv Arguments
 */
using namespace std;

int main(int argc, char **argv) {

	if(string(argv[1]) == "read")
	{
		fstream fs;
		fs.open(string("./") + argv[2], ios_base::in);

		if (!fs.is_open()) // если файл не открыт
			cout << "File does not exist\n"; // сообщить об этом
		else
    		{
			string line;
			while (getline(fs, line))
        		{
            			std::cout << line << std::endl;
        		}
			fs.close(); // закрываем файл
    		}
	}
	else if(string(argv[1]) == "write")
	{
		fstream fs;
		fs.open(string("./") + argv[2], ios_base::app);
                if (!fs.is_open()) // если файл не открыт
                        cout << "File does not exist\n"; // сообщить об этом
                else
                {
                        fs << argv[3] << std::endl;
                        fs.close(); // закрываем файл
			cout << "success" << endl;
                }
	}
	else if(string(argv[1]) == "delete")
	{
		remove(argv[2]);
		cout << "success" << endl;
	}
	else
	{
		cout << "fffffff" << argv[1] << "fffffffff" << endl;
	}
}

